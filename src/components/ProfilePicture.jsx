import React from "react";

const ProfilePicture = ({ profile_image }) => {

    //Random number between 1 and 30 (both included)
    const randomeNumber = Math.floor(Math.random() * 30) + 1;

    //If profile_image is null it adds a random png url as src
    return <img src={profile_image ? profile_image : `images/monster_${randomeNumber}.png`} alt="profile-avatar" />
}

export default ProfilePicture;
import React from "react";
import EmployeeListItem from "./EmployeeListItem";
import "../styling/employeeList.css"

const EmployeeList = ({ data }) => {

    const displayEmployees = data.map(employee => <EmployeeListItem key={employee.id} employee={employee} />)

    return (
        <div className="employee-list">
            <header className="employee-list-header">

                <div className="group-small">
                    <div>Avatar</div>
                    <div>Id</div>
                </div>

                <div>Name</div>
                <div>Salary</div>
                <div>Age</div>
            </header>

            {data && displayEmployees}

        </div>
    );
}

export default EmployeeList;
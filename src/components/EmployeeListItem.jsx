import React from "react";
import "../styling/employeeListItem.css"
import ProfilePicture from "./ProfilePicture";

const EmployeeItem = ({ employee }) => {

    const { id, employee_name, employee_salary, employee_age, profile_image } = employee;

    return (
        <div className="employee-list-item">
            <div className="group-small">
                <div><ProfilePicture profile_image={profile_image} /></div>
                <div>{id}</div>
            </div>
            <div>{employee_name}</div>
            <div>{new Intl.NumberFormat('sv-SV', { style: 'currency', currency: 'SEK' }).format(employee_salary)}</div>
            <div>{employee_age}</div>
        </div>
    );
}

export default EmployeeItem;
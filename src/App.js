import './styling/footer.css'
import Employees from './views/Employees';

function App() {
  return (
    <div className="App">
      <Employees />
      <footer>
        <p>
          Placeholder avatars provided by:
          <a href="https://www.flaticon.com/free-icons/monster" title="monster icons"> Monster icons created by Smashicons - Flaticon</a>
        </p>
      </footer>
    </div>
  );
}

export default App;

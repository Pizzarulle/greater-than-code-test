import React, { useLayoutEffect, useState } from "react";
import { fetchEmployees } from "../api/employees";
import EmployeeList from "../components/EmployeeList";
import "./../styling/employees.css"

const Employees = () => {

    const [employees, setEmplyees] = useState([])
    const [isLoading, setLoading] = useState(true)
    const [errorMessage, setErrorMessge] = useState(null)

   //Fetches employees when component is rendering
    useLayoutEffect(() => {

        //useLayoutEffect cant be async therefor this function exists
        const testFetch = async () => {
            setLoading(true)

            const [potentialError, responseData] = await fetchEmployees()
            
            if (potentialError !== null) {
                setErrorMessge(potentialError)
            } else {
                setEmplyees(responseData)
            }

            setLoading(false)
        }

        testFetch()
        
    }, [setEmplyees, setLoading, setErrorMessge])

    return (
        <div>
            {(employees !== null) && <EmployeeList data={employees} />}

            {errorMessage && employees && <p className="info-message error-text">{errorMessage}</p>}
            {isLoading && <p className="info-message">Fetching Employees...</p>}
        </div>
    );
}

export default Employees;
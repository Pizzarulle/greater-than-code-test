import { API_URL } from "../constants/constants";

/**
 * Fetches employees from api
 * @returns [Error Message, All Employees]
 */
export const fetchEmployees = async () => {
    try {
        const response = await fetch(API_URL)

        if (!response.ok) {
            throw new Error(response.statusText + ". Please wait a few seconds and reload.")
        }
        const { data } = await response.json();

        return [null, await data]
    } catch (error) {
        return [error.message, []]
    }
}
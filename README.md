# Greater-Than-Code-Test

ReactJs web app that displays data from an API in a list that looks good.

If error text appears just refresh the page until it works. This is due to an "429 Too Many Requests" response from the API. 

# Demo link
https://ahells-greater-than-code-test.herokuapp.com/

Give the heroku site a moment to load. Its slow.

## Install

In the root of the folder open a terminal or powershell window and run:

```sh
npm install
```

## Usage

In the root of the folder open a terminal or powershell window and run:

```sh
npm start
```

Further instructions will appear in your console. Leave the window open while in use.

